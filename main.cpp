#include <SFML/Graphics.hpp>
#include <Renderer.hpp>
#include "Player.hpp"
#include <iostream>
#include <sstream>

const sf::Uint8* getPixels(const sf::Image& img, const sf::IntRect& rect);
std::vector<std::vector<sf::Uint8>> pxs;
int main()
{
    Player p;
    p.setPosition(11.5f, 10.5f);
    Map m(sf::Vector2u(24U, 24U));

    try
    {
        if(!m.loadFromFile("map.map"))
            return 1;
    }
    catch(const char* a)
    {
        std::cout << a << std::endl;
    }

    sf::Image img;
    img.loadFromFile("/home/matthis/Bilder/the folder./27856.png");

    std::map<unsigned int, const sf::Uint8*> imgs;
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(0U, getPixels(img, sf::IntRect(0, 0, 64, 64))));
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(1U, getPixels(img, sf::IntRect(128, 0, 64, 64))));
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(2U, getPixels(img, sf::IntRect(256, 192, 64, 64))));
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(3U, getPixels(img, sf::IntRect(128, 256, 64, 64))));
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(4U, getPixels(img, sf::IntRect(128, 896, 64, 64))));
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(5U, getPixels(img, sf::IntRect(128, 704, 64, 64))));
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(6U, getPixels(img, sf::IntRect(128, 640, 64, 64))));
    imgs.insert(std::map<unsigned int, const sf::Uint8*>::value_type(7U, getPixels(img, sf::IntRect(256, 256, 64, 64))));


    m.setColor(0U, sf::Color::Red);
    m.setWallData(1U, imgs[7U], sf::Vector2u(64, 64));
    m.setWallData(2U, imgs[6U], sf::Vector2u(64, 64));
    m.setWallData(3U, imgs[5U], sf::Vector2u(64, 64));
    m.setWallData(4U, imgs[4U], sf::Vector2u(64, 64));
    m.setWallData(5U, imgs[0U], sf::Vector2u(64, 64));
    m.setWallData(6U, imgs[1U], sf::Vector2u(64, 64));
    m.setWallData(7U, imgs[2U], sf::Vector2u(64, 64));
    m.setWallData(8U, imgs[3U], sf::Vector2u(64, 64));

    Renderer r(m, p, sf::Vector2u(960U, 540U));
    sf::RenderWindow RenderApp(sf::VideoMode(960, 540, 32), "Raycast");
    //RenderApp.setVerticalSyncEnabled(true);

    sf::Font font;
    if(!font.loadFromFile("Wizard Of The Moon.ttf")) ///Roland Huse
        return 1;

    sf::Text text("FPS: 0", font, 16U);
    text.setColor(sf::Color::Black);

    int lastfps=0;//time = time * 0.9 + last_frame * 0.1
    int fps=0;
    sf::Clock clock; sf::Clock fpsclock;
    while(RenderApp.isOpen())
    {
        sf::Time step = clock.restart();
        sf::Event event;
        while(RenderApp.pollEvent(event))
        {
            if(event.type == sf::Event::Closed)
            {
                RenderApp.close();
            }
        }

        p.update(step, m);

        RenderApp.clear();
        RenderApp.draw(r);
        RenderApp.draw(text);
        RenderApp.display();

        if(fpsclock.getElapsedTime().asSeconds()>1.f)
        {
            lastfps = fps;
            fps=0;

            std::stringstream ss;
            ss << lastfps;
            text.setString("FPS: "+ss.str());

            fpsclock.restart();
        }
        ++fps;
    }

    return 0;
}

const sf::Uint8* getPixels(const sf::Image& img, const sf::IntRect& rect)
{
    pxs.push_back(std::vector<sf::Uint8>());
    const sf::Uint8* temp = img.getPixelsPtr();

    for(int y=rect.top; y<rect.top+rect.height; ++y)
    {
        for(int x=rect.left; x<rect.left+rect.width; ++x)
        {
            for(unsigned int i=0U; i<4U; ++i)
                pxs[pxs.size()-1].push_back(temp[(x+y*img.getSize().x)*4+i]);
        }
    }

    return &pxs[pxs.size()-1][0];
}
