#include "Game.hpp"
#include <SFML/Graphics/RenderTarget.hpp>

Game::Game(sf::Vector2u size) : p(), m(sf::Vector2u()), r(m, p, size)
{
    p.setPosition(15.f, 15.f);
    try
    {
        m.loadFromFile("map.map");
    }
    catch(const char* a)
    {
        throw a;
    }
}

void Game::operator()(const sf::Time& step)
{
    p.update(step, m);
    r.update();
}

void Game::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(r, states);
}
