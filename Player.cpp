#include "Player.hpp"
#include "Map.hpp"
#include <SFML/System/Time.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>
#include <cmath>

Player::Player()
{

}

void Player::update(const sf::Time& step, Map& m)
{
    if((sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::D)))
    {
        int a = (sf::Keyboard::isKeyPressed(sf::Keyboard::A))?1:-1;
        sf::Vector2f _dir(dir);
        float alpha = ((a * 75.f) * 3.1415926535f/180.f) * step.asSeconds();
        dir.x = _dir.x * std::cos(alpha) - _dir.y * std::sin(alpha);
        dir.y = _dir.x * std::sin(alpha) + _dir.y * std::cos(alpha);

        sf::Vector2f _plane(plane);
        plane.x = _plane.x * std::cos(alpha) - _plane.y * std::sin(alpha);
        plane.y = _plane.x * std::sin(alpha) + _plane.y * std::cos(alpha);
    }
    if((sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Keyboard::isKeyPressed(sf::Keyboard::S)))
    {
        int a = (sf::Keyboard::isKeyPressed(sf::Keyboard::W))?1:-1;
        sf::Vector2f temp(pos+(dir+plane)*5.f*step.asSeconds()*static_cast<float>(a));

        if(m(static_cast<int>(temp.x) + static_cast<int>(temp.y)*m.size().x) == 0U)
        {
            pos = temp;
        }
    }
}
