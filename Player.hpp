#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED

#include <PlayerObject.hpp>
#include <SFML/System/Clock.hpp>

class Map;
namespace sf{class Time;class Window;}

class Player : public PlayerObject
{
public:
    Player();

    void update(const sf::Time& step, Map& m);

private:
};

#endif // PLAYER_HPP_INCLUDED
