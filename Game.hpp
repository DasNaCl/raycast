#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include "Player.hpp"
#include <Renderer.hpp>
#include <Map.hpp>

class Game : public sf::Drawable
{
public:
    Game(sf::Vector2u size);

    void operator()(const sf::Time& step);
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
    Player p;
    Map m;
    Renderer r;
};

#endif // GAME_HPP_INCLUDED
